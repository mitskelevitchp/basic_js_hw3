// Теоретичні питання
// 1. Цикли дозволяють повторювати одні й ті ж самі операції необхідну кількість разів без повторення коду. Це важливо у випадках, коли потрібно опрацювати велику кількість даних. Наприклад, знайти значення Х в масиві (об'єкті), що складається з У значень (мультфільм "Бембі" в базі даних сайту). Або отримати потрубну інформацію з усієї кількості даних, введених користувачем.
// 2. Цикл while зручно використовувати для операцій, які потрібно повторювти n-нну кількість разів, допоки не отримаємо необхідні дані (завдання з запитом користувача (prompt), поки він не ввів потрібні числа). Цикл for - при переборі рядків, числових рядів, даних масиву тощо (завдання з перебором введених користувачем чисел для пошуку цілого числа, кратного заданому).
// 3. Явне здійснюється методами, які застосовуються саме з метою привести дані до іншого типу (наприклад, строку в число за допомогою функції parseFloat). Неявні відбуваються ніби самі собою і тому про них потрібно знати. Наприклад, число, введене користувачем у вікно prompt, отримується як строка. Якщо ми його в подальших розрахунках додаєм до інших чисел, в підсумку також ризикуємо отримати строку. Тому використовуємо унарний "+". І т.д.

// Практичне завдання
// Основне завдання. Цілі числа, кратні 5.
// За умови введення лише додатних чисел
const goal = 5;
const number = prompt("Введіть будь-яке ціле число:");
if (number >= goal) {
  for (let i = goal; i <= number; i++) {
    if (i % goal === 0) {
      console.log(i);
    }
  }
} else if (number < goal) {
  console.log("Вибачте, для вас чисел немає...");
}
alert("Дивіться у консолі");

// За умови введення й від'ємних чисел
// const number = prompt("Введіть будь-яке ціле число:");
// if (number >= 5) {
//   for (let i = 5; i <= number; i++) {
//     if (i % 5 === 0) {
//       console.log(i);
//     }
//   }
// } else if (number <= -5) {
//   for (let i = -5; i >= number; i--) {
//     if (i % 5 === 0) {
//       console.log(i);
//     }
//   }
// } else if (number > -5 || number < 5) {
//   console.log("Вибачте, для вас чисел немає...");
// }

// ДОДАТКОВЕ завдання. За умови введення додатних та виведення в консоль лише при введенні цілих чисел
// while (true) {
//   const goal = 5;
//   const number = prompt("Введіть будь-яке ціле число:");
//   if (number >= goal) {
//     for (let i = 5; i <= number; i++) {
//       if (i % goal === 0 && number % 1 === 0) {
//         console.log(i);
//       }
//     }
//   } else if (number < goal) {
//     console.log("Вибачте, для вас чисел немає...");
//   }

//   if (number % 1 === 0) break;
// }

// ДОДАТКОВЕ завдання. За умови введення додатних та від'ємних й виведення в консоль лише при введенні цілих чисел
// while (true) {
//   const number = prompt("Введіть будь-яке ціле число:");
//   if (number >= 5 && number % 1 === 0) {
//     for (let i = 5; i <= number; i++) {
//       if (i % 5 === 0) {
//         console.log(i);
//       }
//     }
//   } else if (number <= -5 && number % 1 === 0) {
//     for (let i = -5; i >= number; i--) {
//       if (i % 5 === 0) {
//         console.log(i);
//       }
//     }
//   } else if (number > -5 || number < 5) {
//     console.log("Вибачте, для вас чисел немає...");
//   }

//   if (number % 1 === 0) break;
// }

// ДОДАТКОВЕ завдання. Прості числа від малого до великого
let question = confirm("Бажаєте продовжити? :)");
if (question === false) {
  alert("Бувайте здорові!");
} else if (question === true) {
  console.log('Додаткове завдання "Прості числа":');
  question = alert("Завдання 2: Прості числа");
  let firstNumber = +prompt("Введіть 1 будь-яке число:");
  let secondNumber = +prompt("Введіть 2 будь-яке число:");
  let maxNumber = 0;
  if (firstNumber > 1 && secondNumber > 1 && firstNumber !== secondNumber) {
    if (firstNumber > secondNumber) {
      maxNumber = firstNumber;
    } else if (firstNumber < secondNumber) {
      maxNumber = secondNumber;
    }

    // перевірка на просте
    if (secondNumber > firstNumber) {
      start: for (let i = firstNumber; i <= secondNumber; i++) {
        for (let j = 2; j < i; j++) {
          if (i % j === 0) continue start;
        }
        console.log(i);
      }
      alert("Дивіться у консолі");
    } else if (firstNumber > secondNumber) {
      start: for (let i = secondNumber; i <= firstNumber; i++) {
        for (let j = 2; j < i; j++) {
          if (i % j === 0) continue start;
        }
        console.log(i);
      }
      alert("Дивіться у консолі");
    }
  } else
    while (true) {
      // В разі помилкового введення
      alert("Помилка! Спробуй ще раз!");
      firstNumber = +prompt("Введіть 1 будь-яке число:");
      secondNumber = +prompt("Введіть 2 будь-яке число:");
      if (firstNumber > 1 && secondNumber > 1 && firstNumber !== secondNumber) {
        if (firstNumber > secondNumber) {
          maxNumber = firstNumber;
        } else if (firstNumber < secondNumber) {
          maxNumber = secondNumber;
        }

        // перевірка на просте
        if (secondNumber > firstNumber) {
          start: for (let i = firstNumber; i <= secondNumber; i++) {
            for (let j = 2; j < i; j++) {
              if (i % j === 0) continue start;
            }
            console.log(i);
          }
          alert("Дивіться у консолі");
        } else if (firstNumber > secondNumber) {
          start: for (let i = secondNumber; i <= firstNumber; i++) {
            for (let j = 2; j < i; j++) {
              if (i % j === 0) continue start;
            }
            console.log(i);
          }
          alert("Дивіться у консолі");
        }
        break;
      }
    }
}
//
